// import "babel-polyfill"
import assert from "power-assert"
import _ from "ramda"
import {sonarStatsP} from "./sonar-stats"
import {exec, isNotEmpty, isNotNil} from "./util"


export function fileStats(filePath) {
  // const filePath = process.argv[2]
  
  // console.log(process.argv)
  
  assert(isNotNil(filePath))
  assert(isNotEmpty(filePath))
  
  
  const referencedInIssuesCountP = exec(`git log --format="%B" --follow  -- ${filePath}`, {timeout: 2 * 1000})
      .then(stdout => {
        // console.log("stdout", stdout)
        const regExp = /((?:fixes|fix) ?#(\d+))/mgi
        const matchList = stdout.match(regExp)
        if (!matchList) {
          return 0
        }
        // console.log(matchList)
        // noinspection UnnecessaryLocalVariableJS
        const uniqueIssueCount = _.compose(
            _.length,
            _.uniq,
            _.map(_.compose(_.nth(2), (str) => new RegExp(regExp).exec(str))),
        )(matchList)
        // console.log(uniqueIssueCount)
        return uniqueIssueCount
      })
  
  
  const churnP = exec(`git log --pretty=oneline --follow  -- ${filePath}`, {timeout: 2 * 1000})
      .then(stdout => {
        // console.log("stdout", stdout)
        // noinspection UnnecessaryLocalVariableJS
        const churn = stdout.split("\n").length
        // console.log(churn)
        return churn
      })
  
  
  const sonarP = sonarStatsP(filePath)
  
  
  return Promise.all([referencedInIssuesCountP, churnP, sonarP])
                .then(([issueCount, churn, sonarMeasures]) =>
                    _.concat(
                        [createMeasure("filePath", filePath),
                         createMeasure("issueCount", issueCount),
                         createMeasure("churn", churn),
                        ],
                    )(sonarMeasures),
                )
                .then(addRiskMeasure)
  
}

function createMeasure(name, value) {
  return {metric: name, value}
}


function addRiskMeasure(measures) {
  const findMeasureFloatValue = metricKey => _.compose(parseFloat,
      _.prop("value"),
      _.find(_.propEq("metric", metricKey)),
  )
  const securityRating = findMeasureFloatValue("security_rating")(measures)
  const reliabilityRating = findMeasureFloatValue("reliability_rating")(measures)
  const maintainabilityRating = findMeasureFloatValue("sqale_rating")(measures)
  const complexity = findMeasureFloatValue("complexity")(measures)
  const ncloc = findMeasureFloatValue("ncloc")(measures)
  const duplicationRatio = findMeasureFloatValue("duplicated_lines_density")(measures) / 100
  
  const complexityRating = computeComplexityRating(complexity, ncloc)
  const complexityMeasure = createMeasure("complexity_rating", complexityRating)
  
  const duplicationRating = computeDuplicationRating(duplicationRatio)
  const duplicationMeasure = createMeasure("duplication_rating", duplicationRating)
  
  
  const ratings = [securityRating, reliabilityRating, maintainabilityRating, complexityRating, duplicationRating]
  console.log(ratings)
  
  const riskRating = _.reduce(_.max, 0)(ratings)
  
  const riskMeasure = createMeasure("risk_rating", riskRating)
  return _.concat([riskMeasure, complexityMeasure, duplicationMeasure])(measures)
}


function computeComplexityRating(complexity, ncloc) {
  const complexityRatio = complexity / ncloc
  if (complexityRatio <= 0.05) {
    return 1
  } else if (complexityRatio <= 0.10) {
    return 2
  } else if (complexityRatio <= 0.20) {
    return 3
  } else if (complexityRatio <= 0.30) {
    return 4
  } else {
    return 5
  }
}

function computeDuplicationRating(duplicationRatio) {
  if (duplicationRatio <= 0.10) {
    return 1
  } else if (duplicationRatio <= 0.30) {
    return 2
  } else if (duplicationRatio <= 0.60) {
    return 3
  } else if (duplicationRatio <= 0.80) {
    return 4
  } else {
    return 5
  }
}
