import fetch from "node-fetch"
import _ from "ramda"

const metricKeys = [
  "security_rating",
  "reliability_rating",
  "sqale_rating",
  "bugs",
  "vulnerabilities",
  "duplicated_lines_density",
  "coverage",
  "ncloc",
  "complexity",
  "sqale_index",
]

const metricKeysAsString = _.join(",")(metricKeys)

function findIndexOfMeasure(measure) {
  return _.findIndex(_.equals(measure.metric))(metricKeys)
}


export function sonarStatsP(filePath) {
  // const SONAR_URL = "http://localhost:9000"
  const projectKey = process.env["SONAR_MERGE_REQUEST_PROJECT_NAME"]
  const SONAR_URL = "http://sonarqube:9000"
  return fetch(`${SONAR_URL}/api/measures/component?componentKey=${projectKey}:${filePath}&metricKeys=${metricKeysAsString}`)
      .then(async (res) => {
        const json = await  res.json()
        return {res, json}
      })
      .then(({res, json}) => {
        // console.log("json", json)
        if (res.ok) {
          const measures = (json["component"]["measures"])
          return _.sortBy(findIndexOfMeasure)(measures)
        } else {
          return _.map(metricKey => ({"metric": metricKey, value: "NewFile"}))(metricKeys)
        }
      })
  
}

