import fetch from "node-fetch"
import assert from "power-assert"
import _ from "ramda"
import {fileStats} from "./file-stats"
import {exec, isNotEmpty, isNotNil} from "./util"
import FormData from "form-data"

// const filePath = process.argv[2]
// console.log("filePath",filePath)

const commitSHA = process.env["CI_COMMIT_SHA"] || process.env["ghprbActualCommit"] || "HEAD"
console.log("commitSHA", commitSHA)

const userToken = process.env["USER_TOKEN"] || process.env["SONAR_GITHUB_TOKEN"] || "8SjxNAv2v8j7M-iEihn4"
console.log("userToken", userToken)
const projectID = process.env["CI_PROJECT_ID"] || 1
console.log("projectID", projectID)


const pullId = process.env["ghprbPullId"]

function intersperseAndSurround(seperator) {
  return _.compose(_.join(""), _.prepend(seperator), _.append(seperator), _.intersperse(seperator))
}

async function githubPostComment(commentBody) {
  
  const res = await fetch(`https://jigargosar:${userToken}@api.github.com/repos/jigargosar/java-code-doctor-sample/issues/${pullId}/comments`,
      {
        method: "POST", body: JSON.stringify({
        body: commentBody,
      }),
      },
  )
  if (res.status < 200 || res.status >= 300) {
    throw new Error(JSON.stringify(_.omit(["body", "headers"], res), null, 2))
  }
  return res.json()
}

async function gitlabPostComment(commentBody) {
  
  const formData = new FormData()
  
  formData.append("private_token", userToken)
  formData.append("note", commentBody)
  
  const res = await fetch(`http://gitlab/api/v4/projects/${projectID}/repository/commits/${commitSHA}/comments`,
      {
        method: "POST", body: formData,
      },
  )
  if (res.status < 200 || res.status >= 300) {
    throw new Error(JSON.stringify(_.omit(["body", "headers"], res), null, 2))
  }
  return res.json()
}


function statsToRiskAdviserComment(stats) {
  // console.log(stats)
  assert(isNotNil(stats))
  assert(isNotEmpty(stats))
  const columns = _.map(_.prop("metric"))(stats[0])
  const header = intersperseAndSurround("|")(columns)
  const headerDivider = _.compose(intersperseAndSurround("|"), _.repeat("----"), _.length)(columns)
  const headerRows = [header, headerDivider]
  
  const rows = _.map(
      _.compose(
          intersperseAndSurround("|"),
          _.map(_.prop("value")),
      ),
  )(stats)
  
  
  const allRows = _.concat(headerRows, rows)
  return _.join("\n")(allRows)
}

exec(`git diff-tree --no-commit-id --name-only -r ${commitSHA}`)
    .then(_.compose(_.filter(isNotEmpty), _.split("\n")))
    .then((files) => {
      console.log("files", files)
      assert(isNotNil(files))
      assert(isNotEmpty(files))
      return Promise.all(_.map(fileStats)(files))
    })
    .then(statsToRiskAdviserComment)
    .then(_.tap(console.log))
    // .then(githubPostComment)
    .then(gitlabPostComment)
    // .then(_.tap(console.log))
    .catch(abortOnError)


function abortOnError(err) {
  if (err) {
    // console.log("Error", _.omit("stack", err))
    console.log("Exiting on err", err)
    process.exit(1)
  }
}
