import * as cp from "child_process"
import _ from "ramda"

export const isNotNil = _.compose(_.not, _.isNil)
export const isNotEmpty = _.compose(_.not, _.isEmpty)

export function exec(...args) {
  return new Promise((resolve, reject) => {
    cp.exec(...args, function (err, stdout, stderr) {
      if (err) {
        reject(err)
      } else if (!_.isEmpty(stderr)) {
        console.log("stderr", stderr)
        reject(stderr)
      }
      else {
        resolve(stdout)
      }
    })
  })
}

