

* Install Docker for mac


> docker-compose up -d 


* check logs for specific container:

> docker-compose logs -f gitlab

* Once all service are up run following command, to register the runner with gitlab.

> docker-compose exec gitlab-runner gitlab-runner register -u 'http://gitlab' -r token --executor docker --docker-image 'maven:latest' --docker-network-mode 'pullrequestdoctor_default' --docker-pull-policy 'if-not-present' --docker-volumes "gitlab_runner_cache:/cache" --docker-volumes  "gitlab_runner_m2:/root/.m2" -n


* From Intellij idea, run the shared run config to create java+node docker image. It will be required to build our java-code-doctor-sample project, from within gitlab-runner.
  


  

Old Setup:

* yarn install 


* https://docs.google.com/document/d/17Qye219SC8uCmXJUZFP5QL1p8oth8cqJqbv7libUdwk
