FROM node:8.5-alpine

RUN apk add --no-cache git

RUN mkdir -p /code

COPY build/main.js /code/
