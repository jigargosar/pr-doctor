import "babel-polyfill"
import {run} from 'runjs'
import * as _ from "ramda"
import * as assert from "power-assert"

export function dummy(){
  run("echo dummy")
}

const isNotNil = _.compose(_.not, _.isNil)

export const git = {
  commitCount(filePath){
    assert(isNotNil(filePath))
    const lines = run(`git log --pretty=oneline --follow  -- ${filePath}`, {stdio: 'pipe'})
    console.log(lines.split("\n").length)
  }
}

